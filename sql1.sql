-- Active: 1670761295533@@127.0.0.1@5434@main


create table weather
(
    city    varchar(60),
    temp_lo int,
    temp_hi int,
    prcp    real,
    date    date
);

create table cities
(
    name     varchar(60),
    location point
);

insert into weather
VALUES ('San Francisco', 46, 50, 0.25, '1994-11-27');

insert into weather (city, temp_lo, temp_hi, prcp, date)
values ('San Francisco', 43, 57, 0.0, '1994-11-29');

insert into cities
VALUES ('San Francisco', '(-194.0, 53.0)');

insert into weather (date, city, temp_hi, temp_lo)
values ('1994-11-29', 'Hayward', 54, 37);

select *
from weather;

select city, (temp_lo + weather.temp_hi) / 2 as temp_avg
from weather;

select *
from weather
where city = 'San Francisco'
  and prcp > 0.0;

select distinct city
from weather;


select *
from weather w
         join cities c on w.city = c.name;

select *
from weather w
         left join cities c on w.city = c.name;

select max(temp_lo)
from weather;

select city
from weather w
where w.temp_lo = (select max(temp_lo) from weather);

select city, max(temp_lo)
from weather
group by city;

select city, max(temp_lo)
from weather
where city ILIKE 'S%'
group by city
having max(temp_lo) < 40;


select email
from your_view;
select *
from my_view;



select *
from cities;

select count(location)
from cities;

insert into cities
values ('Mackhachkala');

drop table if exists products;

create table products
(
    product_no serial,
    price      numeric default 9.99,
    discont    numeric generated always as (price / 10) stored
);

insert into product (price)
values (99.12),
       (-21),
       (320);
select *
from products;

drop table products;

create table if not exists product
(
    product_no serial,
    price      numeric
        constraint const_price check (price > 0)

);

create table if not exists product
(
    product_no     serial,
    price          numeric check ( price > 0 ),
    discount_price numeric check ( discount_price > 0 ),
    check ( discount_price < price )
);

create table if not exists product
(
    product_no       serial,
    price            numeric,
    check ( price > 0 ),
    discounted_price decimal,
    check ( discounted_price > 0 ),
    check ( price > discounted_price )
);

create table if not exists product
(
    product_no       serial                                 not null,
    price            numeric                                not null,
    check ( price > 0 ),
    name             text default 'UNDEFINED'               null,
    discounted_price decimal check ( discounted_price > 0 ) not null,
    constraint valid_discounted check ( price > discounted_price )
);



select *
from weather;

select *, max(temp_hi) over (partition by date) as temp, (temp_hi - w.temp_lo) as different
from weather w;



drop table if exists product;

create table products
(
    product_no integer primary key,
    name       varchar(100),
    price      numeric,
    constraint check_price check ( price > 0 )
);


create table orders
(
    order_id         integer primary key,
    shipping_address text
);

create table order_items
(
    product_no integer references products (product_no),
    order_id   integer references orders (order_id),
    quantity   integer,
    primary key (product_no, order_id)
);

insert into products (product_no, name, price)
values (1, 'first', 500);
insert into orders (order_id, shipping_address)
values (1, 'Untsukul');
insert into order_items (product_no, order_id, quantity)
VALUES (1, 1, 5);



create table if not exists author
(
    author_id  int primary key,
    first_name varchar(64) not null
);

create table if not exists book
(
    book_id int primary key,
    title   varchar(124) not null
);

create table author_book
(
    book_id   int references book (book_id),
    author_id int references author (author_id),
    constraint book_and_author_pkey primary key (book_id, author_id)
);

insert into book
values (1, 'Коротко обо всем');
insert into author
values (1, 'Lesha');
insert into author_book
values (1, 1);
select *
from author_book;

select count(distinct city) as countUniqueCity
from employees;



select count(distinct o.ship_city)
from orders o;



select contact_name, city
from customers;
select *
from orders;

select o.order_id, o.order_date - o.shipped_date as diff
from orders o;

select *
from customers;
select count(distinct (c.city))
from customers c;

select distinct c.country, c.city
from customers c;

select count(distinct (c.country, c.city))
from customers c;

select count(*)
from customers;
select count(distinct (c.country))
from customers c;


select *
from products
where unit_price > 25
  and units_in_stock < 40;

select *
from customers
where city = 'Berlin'
   or city = 'London'
   or city = 'San Francisco'
order by city;

select *
from orders o
where o.shipped_date > '1998-04-30'
  and (o.freight < 75 or o.freight > 150)
order by o.freight desc;

select *
from orders
where freight >= 20
  and freight <= 40;

select count(*)
from orders
where freight between 20 and 40;

select *
from orders
where freight > 40;

select *
from orders
where order_date between '1998-03-30' and '1998-04-05';

select *
from customers c
where c.country = 'Mexico'
   or c.country = 'USA'
   or c.country = 'Germany'
   or c.country = 'Canada';

select count(*)
from customers c
where c.country IN ('Canada', 'USA', 'Germany', 'Mexico');

select count(*)
from customers c
where c.country not in ('Canada', 'USA', 'Germany', 'Mexico');

select *
from products
where category_id in (1, 2, 3, 5, 7);

select *
from products
where category_id not in (1, 2, 3, 5, 7);

select distinct country, city
from customers
order by country desc, city desc;

select ship_city, order_date
from orders
where ship_city = 'London'
order by order_date asc
limit 1;

select min(order_date)
from orders
where ship_city = 'London';

select max(order_date)
from orders
where ship_city = 'London';

select avg(unit_price)
from products p
where p.discontinued <> 1;

select sum(units_in_stock)
from products
where discontinued <> 1;

select *
from orders o
where o.ship_country in ('France', 'Austria', 'Span');

select *
from orders
order by required_date desc, shipped_date asc;


select min(units_in_stock)
from products p
where p.units_in_stock > 30;

select max(units_in_stock)
from products p
where p.units_in_stock > 30;

select min(required_date - order_date)
from orders
where ship_country = 'USA';



select sum(unit_price * units_in_stock)
from products
where discontinued <> 1;

select *
from orders
where ship_region is not null;

select *
from orders
where ship_region is null;

select ship_country, count(*)
from orders
where freight > 50
group by ship_country
order by ship_country desc;

select category_id, sum(units_in_stock)
from products
group by category_id
order by sum(units_in_stock) desc;


select category_id, sum(unit_price * units_in_stock)
from products
where discontinued <> 1
group by category_id
having sum(unit_price * units_in_stock) > 5000
order by sum(unit_price * units_in_stock) desc;

select c.city, c.country
from customers c
union
select e.city, e.country
from employees e;

select c.country
from customers c
union
select e.country
from employees e;

select c.country
from customers c
intersect
select s.country
from suppliers s;

select country
from customers
except
select country
from suppliers;

select country
from customers
except all
select country
from suppliers;

select *
from orders o
where o.ship_country like 'U%';

select o.order_id, o.customer_id, o.freight
from orders o
where o.ship_country like 'N%'
order by o.freight desc
limit 10;

select e.first_name, e.last_name, e.home_phone, e.region
from employees e
where e.region is null;

select count(*)
from customers
where region is not null;

select s.country, count(*)
from suppliers s
group by s.country
order by count(*) desc;

select o.ship_country, sum(o.freight)
from orders o
where o.ship_region is not null
group by o.ship_country
having sum(o.freight) > 2750
order by sum(o.freight);

select c.country
from customers c
union
select s.country
from suppliers s
order by country asc ;

select c.country
from customers c
intersect
select s.country
from suppliers s
intersect
select e.country
from employees e;

select c.country
from customers c
intersect
select s.country
from suppliers s
except
select e.country
from employees e;

