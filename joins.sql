select p.product_name, s.company_name, p.units_in_stock
from products p
inner join suppliers s on p.supplier_id = s.supplier_id
order by units_in_stock desc ;

select *
from products p
inner join suppliers s on p.supplier_id = s.supplier_id;


select category_name, sum(units_in_stock)
from products p
inner join categories c on p.category_id = c.category_id
group by category_name
order by sum(units_in_stock) desc
limit 5;

select category_name, sum(units_in_stock * unit_price)
from products p
inner join categories c on p.category_id = c.category_id
where discontinued <> 1
group by category_name
having sum(unit_price * units_in_stock) > 5000
order by sum(units_in_stock * unit_price) desc ;

select order_id, customer_id, first_name, last_name, title
from orders o
inner join employees e on e.employee_id = o.employee_id
order by first_name desc
limit 20;

select order_date, product_name, ship_country, p.unit_price, quantity, discount
from orders o
inner join order_details od on od.order_id = o.order_id
inner join products p on od.product_id = p.product_id;

select contact_name, company_name, phone, first_name, last_name, title,
       order_date, product_name, ship_country, p.unit_price, quantity, discount
from orders o
join order_details od on o.order_id = od.order_id
join products p on od.product_id = p.product_id
join customers c on o.customer_id = c.customer_id
join employees e on o.employee_id = e.employee_id
where ship_country = 'USA';


select company_name, order_id
from customers c
left join orders o on c.customer_id = o.customer_id
where order_id is not null;

select last_name, order_id
from employees e
left join orders o on e.employee_id = o.employee_id
where order_id is null;

select count(*)
from employees e
left join orders o on e.employee_id = o.employee_id
where o.order_id is not null;


select contact_name, company_name, phone, first_name, last_name, title,
       order_date, product_name, ship_country, p.unit_price, quantity, discount
from orders o
join order_details od using (order_id)
join products p using (product_id)
join customers c using (customer_id)
join employees e using (employee_id)
where ship_country = 'USA';

select category_id, sum(units_in_stock) as total_quantity
from products
group by category_id
order by total_quantity desc
limit 5;

select c.country
from customers c
intersect
select e.country
from employees e;

select c.company_name as customer_company, e.first_name || ' ' || e.last_name as name
from customers c
left join orders o on c.customer_id = o.customer_id
left join employees e on e.employee_id = o.employee_id
where c.country = 'London'
and e.country = 'London'
and o.ship_country = 'Speedy Express';


select p.product_name, p.units_in_stock, s.contact_name, s.phone
from products p
left join categories c using (category_id)
left join suppliers s using (supplier_id)
where p.discontinued <> 1
and p.units_in_stock < 20
and c.category_name in ('Beverages', 'Seafood');

select c.contact_name, o.order_id
from customers c
left join orders o on o.customer_id = c.customer_id
where o.order_id is null;



