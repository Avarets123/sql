select s.company_name
from suppliers s
where country in (select distinct c.country
                  from customers c);

select distinct s.company_name
from suppliers s
         join customers using (country);


select category_name, sum(units_in_stock)
from products p
         join categories c on c.category_id = p.category_id
group by category_name
order by sum(units_in_stock) desc
limit (select min(product_id) + 4 from products);


select p.product_name, p.units_in_stock
from products p
where p.units_in_stock > (select avg(units_in_stock)
                          from products)
order by units_in_stock;


select c.company_name, c.contact_name
from customers c
where exists(select c.customer_id
             from orders o
             where o.customer_id = c.customer_id
               and o.freight between 50 and 100);

select c.company_name, c.contact_name
from customers c
where not exists(select o.customer_id
                 from orders o
                 where c.customer_id = o.customer_id
                   and o.order_date between '1995-02-01' and '1995-02-15');

select distinct c.company_name
from customers c
         join orders o on c.customer_id = o.customer_id
         join order_details od on o.order_id = od.order_id
where od.quantity > 40;


select distinct c.company_name
from customers c
where c.customer_id = any (select o.customer_id
                           from orders o
                                    join order_details od on o.order_id = od.order_id
                           where od.quantity > 40);

select distinct p.product_name, od.quantity
from products p
         join order_details od on p.product_id = od.product_id
where quantity > (select avg(quantity)
                  from order_details)
order by quantity desc;

select avg(quantity)
from order_details od
group by product_id
order by avg(quantity) asc ;

select distinct product_name, quantity
from products
join order_details od on products.product_id = od.product_id
where quantity > all (select avg(quantity)
                      from order_details
                      group by od.product_id)
order by quantity asc ;
